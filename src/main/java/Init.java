import handler.Handler;
import network.Receiver;
import network.Sender;
import state.Sessions;
import system.AckMonitor;
import system.HandlerMonitor;
import system.TimeOutMonitor;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.*;

public class Init {

    private DatagramSocket datagramSocket = null;
    private int port;

    public Init(int port) {
        this.port = port;
        initializeServer();
    }

    public void initializeServer() {

        Sessions sessions = Sessions.getInstance();

        //Create the datagram socket
        try {
            datagramSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        //Set queues for incoming and outgoing messages
        final BlockingQueue<DatagramPacket> messageQueue = new ArrayBlockingQueue<>(20000);
        final BlockingQueue<DatagramPacket> senderQueue = new ArrayBlockingQueue<>(20000);
        //Client client = new Client(port);

        //Create sender and receiver threads
        Sender sender = new Sender(senderQueue, datagramSocket);
        Receiver receiver = new Receiver(messageQueue, datagramSocket);

        TimeOutMonitor timeOutMonitor = TimeOutMonitor.getInstance(senderQueue);

        //Create a thread pool for receiver, sender and master handler
        final ExecutorService executorService = newFixedThreadPool(3);

        //Launch receiver, sender and master handler threads
        executorService.submit(receiver);
        executorService.submit(sender);
        executorService.submit(new Handler(messageQueue, senderQueue, "master"));

        //Start Handlermonitor
        HandlerMonitor handlerMonitor = new HandlerMonitor(messageQueue, senderQueue);
        AckMonitor ackMonitor = AckMonitor.getInstance(senderQueue);
    }

    private static ExecutorService newFixedThreadPool(int nThreads) {
        ThreadPoolExecutor tp = new ThreadPoolExecutor(nThreads, 15, 10, TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>());
        tp.allowCoreThreadTimeOut(false);

        return tp;
    }
}
