package enums;

public enum EventTypes {
        HEARTBEAT("00"),
        LOGIN("01"),
        MOVE("02"),
        TEST("03"),
        ACK("99");


        private String code;

        EventTypes(String code) {
            this.code = code;
        }

        public static String getCode(EventTypes event) {
            for(EventTypes e : EventTypes.values()) {
                if(event == e) {
                    return e.code;

                }
            }

            return "33";
        }

        public static EventTypes fromString(String code) {
            for(EventTypes e : EventTypes.values()) {
                if (e.code.equals(code)) {

                    return e;
                }
            }
            return null;
        }
}
