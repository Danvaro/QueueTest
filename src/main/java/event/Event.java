package event;

import model.Request;

public interface Event {

   void procesEvent(Request request);

}
