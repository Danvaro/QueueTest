package event;

import model.Request;

public class Heartbeat implements Event{

    public Heartbeat(int h) {
        this.h = h;
    }

   int h;

    @Override
    public void procesEvent(Request request) {
        System.out.println("Got the heartbeat!!!");
    }
}
