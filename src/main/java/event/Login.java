package event;

import lombok.Getter;
import model.Request;
import model.Session;
import state.Sessions;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

@Getter
public class Login implements Event {

    private String u;
    private String p;

    @Override
    public void procesEvent(Request request) {
        Sessions.addNewSession(Session.builder()
                .IPadres(request.getSenderIP())
                .port(request.getSenderPort())
                .username(getU())
                .lastMessageReceivedAt(new Date())
                .lastMessageWithAckSendAt(new Date())
                .receivedEventsWithAck(new ConcurrentHashMap<Integer, Date>())
                .sendEventsRequiringAck(new ConcurrentHashMap<Integer, SendAck>())
                .build());

        System.out.println("Username: " +Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort()).getUsername());
    }
}
