package event;

import model.Request;
import state.Sessions;

public class ReceiveAck implements Event {

    int s;

    public ReceiveAck(int sequenceNumber) {
        s = sequenceNumber;
    }

    @Override
    public void procesEvent(Request request) {
        System.out.println("Got ack from: " + request.getSenderIP() + ":" + request.getSenderPort() + " for message " + s);
        Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort()).incrementRemoteSequence();
        System.out.println("Removing ack for seq: " + s);
        Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort()).sendEventsRequiringAck.remove(s);

        System.out.println("ack size: " + Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort()).sendEventsRequiringAck.size());
    }
}
