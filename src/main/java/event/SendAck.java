package event;

import model.Request;

import java.net.DatagramPacket;
import java.util.Date;

public class SendAck implements Event {

    public SendAck(DatagramPacket eventToAck) {
        this.eventToAck = eventToAck;
    }

    public Date sendAckAt = new Date();
    public DatagramPacket eventToAck;

    @Override
    public void procesEvent(Request request) {

    }
}
