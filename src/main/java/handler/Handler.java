package handler;

import enums.EventTypes;
import event.*;
import model.Request;
import model.Session;
import state.Sessions;
import util.PacketFactory;
import util.RequestParser;

import java.net.DatagramPacket;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;


public class Handler implements Runnable {

    private final BlockingQueue<DatagramPacket> messageQueue;
    private final BlockingQueue<DatagramPacket> sendQueue;
    private long queTimeOut = 1000;
    private String role;
    private DatagramPacket datagramPacket;

    public Handler(BlockingQueue<DatagramPacket> messageQueue, BlockingQueue<DatagramPacket> sendQueue, String role) {
        this.messageQueue = messageQueue;
        this.role = role;
        this.sendQueue = sendQueue;
    }

    @Override
    public void run() {
        while (true) {
            // Stops surplus handlers if there is no or very little work to be done
            handlerSurplus();

            //Blocks till there is a message to be handled. Times out after a few seconds if not master handler
            getMessage();

            //If there is no message after timeout stop worker handler
            if(datagramPacket == null) { handlerTimeOut(); return; }

            //Convert packet to request object
            Request request = RequestParser.parseRequest(datagramPacket);

            //Session session = Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort());

            if(request.isReliable()) {
                System.out.println("message has ack");
                if(Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort()) != null) {
                    try {
                        //TODO fix this
                        if(Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort()).getReceivedEventsWithAck().containsKey(request.getSequence())) {
                            System.out.println("Sequence id: " + request.getSequence());
                            System.out.println("Reliable message already handled");
                        } else {
                            System.out.println("Sequence id: " + request.getSequence());
                            System.out.println("new reliable message not yet handled");
                            Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort()).getReceivedEventsWithAck().put(request.getSequence(), new Date());
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }

                    sendQueue.add(PacketFactory.createPacket(false, false, request.getSequence(), EventTypes.ACK, new ReceiveAck(request.getSequence()), request.getSenderIP(), request.getSenderPort()));
                } else {
                    System.out.println("Sessions doesnt exist");
                }

            } else {
                System.out.println("Request is not reliable");
            }

            if(request.isSequenced()) {
                System.out.println("Message is sequenced");
            }

            ((Event) request.getEventObject()).procesEvent(request);
            if(!request.isReliable()) {
                Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort()).setRemoteSequence(request.getSequence());
            }
            Sessions.getSessionByIP(request.getSenderIP().toString() + request.getSenderPort()).setLastMessageReceivedAt(new Date());
            System.out.println("remote sequence:" + request.getSequence());
        }
    }

    private void handlerTimeOut() {
       Thread.interrupted();
    }

    private void handlerSurplus() {
        if (messageQueue.size() < 100 && role.equals("worker")) {
            Thread.currentThread().interrupt();
        }
    }

    private void getMessage() {
        if (role.equals("master")) {
            try {
                datagramPacket = this.messageQueue.take();
            } catch (InterruptedException e) {
               handlerTimeOut();
            }
        } else {
            try {
                datagramPacket = this.messageQueue.poll(queTimeOut, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                handlerTimeOut();
            }
        }
    }
}
