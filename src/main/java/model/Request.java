package model;

import lombok.Builder;
import lombok.Getter;

import java.net.InetAddress;

@Builder
@Getter
public class Request {

    private boolean messageCorrect;

    private InetAddress senderIP;
    private int senderPort;

    private boolean reliable;

    private boolean sequenced;
    private int sequence;

    private Object eventObject;
}
