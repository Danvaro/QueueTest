package model;

import event.SendAck;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.net.InetAddress;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
@Builder
public class Session {

    //Session IP location
    private InetAddress IPadres;
    private int port;
    private int localSequence = 0;
    private int remoteSequence = 0;

    //Player information
    private String username;
    private float x;
    private float y;
    private float z;

    private Date lastMessageReceivedAt;
    private Date lastMessageWithAckSendAt;
    public ConcurrentHashMap<Integer, SendAck> sendEventsRequiringAck;
    public ConcurrentHashMap<Integer, Date> receivedEventsWithAck;

    public void incrementLocalSequence() {
        if(localSequence == 999) {
            localSequence = 0;
        } else {
            localSequence++;
        }
    }

    public void addAck(int seq, SendAck AckContainingEvent) {
        if(this.sendEventsRequiringAck == null) {
            sendEventsRequiringAck = new ConcurrentHashMap<>();
        }
        sendEventsRequiringAck.put(seq, AckContainingEvent);
    }

    public void incrementRemoteSequence() {
        remoteSequence++;
        updateLastMessageReceivedAt();
    }

    public void updateLastMessageReceivedAt() {
        lastMessageReceivedAt = new Date();
    }

    public int getEventsRequiringAckCount() {
        if(sendEventsRequiringAck == null) {
            sendEventsRequiringAck = new ConcurrentHashMap<>();
            return sendEventsRequiringAck.size();
        } else {
            return sendEventsRequiringAck.size();
        }
    }
}
