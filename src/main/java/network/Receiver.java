package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;

public class Receiver implements Runnable {

    private final BlockingQueue<DatagramPacket> messageQueue;
    private DatagramSocket receiverSocket;

    public Receiver(BlockingQueue<DatagramPacket> messageQueue, DatagramSocket datagramSocket) {
        this.messageQueue = messageQueue;
        this.receiverSocket = datagramSocket;
    }

    @Override
    public void run() {

        try {

            System.out.println("Receiver started");
            while (true) {

                byte[] buffer = new byte[500];
                DatagramPacket datagramPacket = new DatagramPacket(buffer, 0, buffer.length);

                receiverSocket.receive(datagramPacket);

                this.messageQueue.put(datagramPacket);
            }
        } catch (SocketException | InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Timeout. Client is closing.");
        }
    }
}
