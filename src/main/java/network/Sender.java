package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.BlockingQueue;

public class Sender implements Runnable {

    private DatagramSocket senderSocket;
    BlockingQueue<DatagramPacket> sendQueue;

    public Sender(BlockingQueue<DatagramPacket> sendQueue, DatagramSocket datagramSocket) {
        this.sendQueue = sendQueue;
        this.senderSocket = datagramSocket;
    }

    @Override
    public void run() {

        try {
            System.out.println("Sender started");
            while(true) {
                DatagramPacket bytesToSend = this.sendQueue.take();
                System.out.println("Sending packet: " + new String(bytesToSend.getData()));
                bytesToSend.getData();
                senderSocket.send(bytesToSend);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
