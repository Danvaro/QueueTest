package network.testclient;


import java.io.IOException;
import java.net.*;

public class Client implements Runnable{

    private final int clientPort;

    public Client(int clientPort) {
        this.clientPort = clientPort;
    }

    @Override
    public void run() {

        try (DatagramSocket serverSocket = new DatagramSocket(clientPort)) {

            for (int i = 0; i < 50000; i++) {
                String message = "31B21121100{\"h\":10 }";
                DatagramPacket datagramPacket = new DatagramPacket(
                        message.getBytes(),
                        message.length(),
                        InetAddress.getLocalHost(),
                        clientPort
                );
                serverSocket.send(datagramPacket);

                Thread.sleep(1);
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
