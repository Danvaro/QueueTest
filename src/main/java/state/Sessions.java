package state;

import model.Session;

import java.util.concurrent.ConcurrentHashMap;

public class Sessions {

    private static Sessions instance = null;
    private Sessions() {}

    public static Sessions getInstance() {
        if(instance == null) {
            instance = new Sessions();
        }
        return instance;
    }

    public static ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();

    public static void addNewSession(Session session) {
        sessions.put(session.getIPadres().toString() + session.getPort(), session);
    }

    public static Session getSessionByIP(String ip) {
        return sessions.get(ip);
    }

    public static void removeSession(String ip) {
        sessions.remove(ip);
    }
}
