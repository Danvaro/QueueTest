package system;

import event.SendAck;
import model.Session;
import state.Sessions;

import java.net.DatagramPacket;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class AckMonitor {

    private static AckMonitor instance = null;
    private BlockingQueue<DatagramPacket> sendQueue;

    public static AckMonitor getInstance(BlockingQueue<DatagramPacket> sendQueue) {
        if(instance == null) {
            return new AckMonitor(sendQueue);
        } else {
            return instance;
        }
    }

    private AckMonitor(BlockingQueue<DatagramPacket> sendQueue) {
        this.sendQueue = sendQueue;
        runAckMonitor();
    }

    private void runAckMonitor() {
        System.out.println("ReceiveAck monitor started");

        //Schedule a timer to add an stop worker threads based upon the amount of incoming messages in the incoming message queue
        Timer t = new Timer();
        t.schedule(new TimerTask() {

            @Override
            public void run() {

                for (Session session: Sessions.sessions.values()) {
                    if(session.getEventsRequiringAckCount() > 0) {
                        for(SendAck eventToAck : session.sendEventsRequiringAck.values()) {
                            if(TimeUnit.MILLISECONDS.toMillis(new Date().getTime() - eventToAck.sendAckAt.getTime()) > 1000) {
                                sendQueue.add(eventToAck.eventToAck);
                                session.setLastMessageWithAckSendAt(new Date());
                                eventToAck.sendAckAt = new Date();
                                System.out.println("sending ack");
                            }
                        }
                    }

                    if(session.getReceivedEventsWithAck().size() > 0) {
                        for (Integer receivedAck : session.getReceivedEventsWithAck().keySet()) {
                            Date ackDate = session.getReceivedEventsWithAck().get(receivedAck);
                            if(TimeUnit.MILLISECONDS.toMillis( new Date().getTime() - ackDate.getTime()) > 5000) {
                                System.out.println("Ack is older than 30 seconds");
                                System.out.println(session.getReceivedEventsWithAck().size());

                                session.getReceivedEventsWithAck().remove(receivedAck);
                            }
                        }
                    }
                }


            }
        }, 0, 50);
    }
}
