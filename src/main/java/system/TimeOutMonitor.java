package system;

import enums.EventTypes;
import event.Heartbeat;
import model.Session;
import state.Sessions;
import util.JsonParser;
import util.PacketFactory;

import java.net.DatagramPacket;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class TimeOutMonitor {

    private BlockingQueue<DatagramPacket> senderQueue;

    private static TimeOutMonitor instance = null;

    public static TimeOutMonitor getInstance(BlockingQueue<DatagramPacket> senderQueue) {
        if (instance == null) {
            return new TimeOutMonitor(senderQueue);
        } else {
            return instance;
        }
    }

    private TimeOutMonitor(BlockingQueue<DatagramPacket> senderQueue) {
        this.senderQueue = senderQueue;
        runTimeOutMonitor();
    }

    private void runTimeOutMonitor() {
        System.out.println("Handler monitor started");

        //Schedule a timer to add an stop worker threads based upon the amount of incoming messages in the incoming message queue
        Timer t = new Timer();
        t.schedule(new TimerTask() {

            @Override
            public void run() {

                Date now = new Date();

                for (Session session : Sessions.sessions.values()) {
                    if (TimeUnit.MILLISECONDS.toMillis(now.getTime() - session.getLastMessageReceivedAt().getTime()) > 5000) {

                        if (TimeUnit.MILLISECONDS.toMillis(now.getTime() - session.getLastMessageReceivedAt().getTime()) > 30000) {
                            Sessions.removeSession(session.getIPadres().toString() + session.getPort());
                            System.out.println("Removing session");
                        } else {
                            if(TimeUnit.MILLISECONDS.toMillis(now.getTime() - session.getLastMessageWithAckSendAt().getTime()) > 5000) {
                                senderQueue.add(PacketFactory.createPacket(true, false, session.getLocalSequence(), EventTypes.HEARTBEAT, new Heartbeat(1), session.getIPadres(), session.getPort()));
                                System.out.println("Sending heartbeat with local sequence: " + session.getLocalSequence());
                            } else {
                                System.out.println("Ack monitor is sending acks, timeout monitor not intervening");
                            }
                        }
                    }
                }

            }
        }, 0, 2000);
    }
}
