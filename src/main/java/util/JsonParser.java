package util;

import com.google.gson.Gson;


public class JsonParser {

    private Gson gson;

    private static JsonParser instance = null;
    private JsonParser() {gson = new Gson();}

    public static JsonParser getInstance() {
        if(instance == null) {
            instance = new JsonParser();
        }
        return instance;
    }

    public <T> T deserialize(String jsonString, Class<T> clazz) {

        return gson.fromJson(jsonString, clazz);
    }

    public String serialize(Object object) {

        return gson.toJson(object);
    }
}
