package util;


import enums.EventTypes;
import event.Event;
import event.SendAck;
import state.Sessions;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Date;

public class PacketFactory {

    public static DatagramPacket createPacket(boolean reliable, boolean sequenced, int sessionSequence, EventTypes eventType, Event eventObject, InetAddress address, int port) {
        String message = PacketInformation.PACKETPROTOCOL;

        String sequence = String.valueOf(sessionSequence);

        if (sequence.length() < 2) {
            sequence = "00" + sequence;
        } else if (sequence.length() < 3) {
            sequence = "0" + sequence;
        }

        if (reliable) {
            message += "1";
        } else {
            message += "0";
        }

        if (sequenced) {
            message += "1" + sequence;
        } else {
            message += "0" + sequence;
        }

        message += EventTypes.getCode(eventType) + JsonParser.getInstance().serialize(eventObject);

        Sessions.getSessionByIP(address.toString() + port).incrementLocalSequence();

        DatagramPacket datagramPacket = new DatagramPacket(
                message.getBytes(),
                message.length(),
                address,
                port
        );

        if(reliable) {
            addPacketToAckList(address.toString(), port, sequence, datagramPacket);
        }

        return datagramPacket;
    }

    private static void addPacketToAckList(String address, int port, String sessionSequence, DatagramPacket datagramPacket) {
        Sessions.getSessionByIP(address + port).addAck(Integer.parseInt(sessionSequence), new SendAck(datagramPacket));
        Sessions.getSessionByIP(address + port).setLastMessageWithAckSendAt(new Date());
    }
}
