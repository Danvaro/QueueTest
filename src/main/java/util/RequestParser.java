package util;

import enums.EventTypes;
import event.ReceiveAck;
import event.Heartbeat;
import event.Login;
import event.Test;
import model.Request;
import org.apache.commons.lang3.StringUtils;

import java.net.DatagramPacket;

public class RequestParser {

    public static Request parseRequest(DatagramPacket datagramPacket) {

        String message = new String(datagramPacket.getData()).trim();

        if (validateMessage(message)) {
            return Request.builder()
                    .messageCorrect(true)
                    .senderIP(datagramPacket.getAddress())
                    .senderPort(datagramPacket.getPort())
                    .reliable(reliabilityCheck(message))
                    .sequenced(sequenceCheck(message))
                    .sequence(getSequence(message))
                    .eventObject(getEventType(message))
                    .build();
        } else {
            System.out.println("Packet is incorrect");
            return Request.builder().messageCorrect(false).build();
        }
    }

    private static boolean validateMessage(String payload) {

        //Check if the payload has the right amount of mandatory bytes
        if(payload.length() < 12) {
            return false;
        }

        //Check if valid protocol
        if (!payload.substring(0, 4).equals("31B2")) {
            return false;
        }

        //Check if reliability byte is numeric
        if (!Character.isDigit(payload.charAt(4))) {
            return false;
        }

        //Check if sequence byte is numeric
        if (!Character.isDigit(payload.charAt(5))) {
            return false;
        }

        //Check if sequence bytes are numeric
        if (!StringUtils.isNumeric(payload.substring(6, 9))) {
            return false;
        }

        //Check if event type is numeric
        if (!StringUtils.isNumeric(payload.substring(9, 11))) {
            return false;
        }

        return true;
    }

    private static boolean reliabilityCheck(String payload) {
        return payload.charAt(4) == '1';
    }

    private static boolean sequenceCheck(String payload) {
        return payload.charAt(5) == '1';
    }

    private static int getSequence(String payload) {
        return Integer.parseInt(payload.substring(6, 9));
    }

    private static Object getEventType(String payload) {
        System.out.println(EventTypes.fromString(payload.substring(9, 11)));
        try {
            switch (EventTypes.fromString(payload.substring(9, 11))) {
                case HEARTBEAT:
                    return JsonParser.getInstance().deserialize(payload.substring(11, payload.length()), Heartbeat.class);
                case LOGIN:
                    return JsonParser.getInstance().deserialize(payload.substring(11, payload.length()), Login.class);
                case TEST:
                    return  JsonParser.getInstance().deserialize(payload.substring(11, payload.length()), Test.class);
                case ACK:
                    return JsonParser.getInstance().deserialize(payload.substring(11, payload.length()), ReceiveAck.class);
                default:
                    return new Fault();
            }
        } catch (Exception e) {
            System.out.println("Failed parsing JSON: " + e);
            return new Fault();
        }
    }
}
