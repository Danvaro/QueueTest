using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class NetworkManager : MonoBehaviour
{
	// Server address
	private string IP = "127.0.0.1";
	public int port = 8192;

	// Receiving packets happens on a different thread
	Thread receiveThread;

	IPEndPoint remoteEndPoint;
	UdpClient client;

	public string lastReceivedUDPPacket = "";
	public string allReceivedUDPPackets = "";

	string strMessage = "";

	// Start the listening thread
	public void Start ()
	{
		StartReceiveThread ();
		init ();
	}

	// Set the remote Endpoint to the server
	public void init ()
	{
		remoteEndPoint = new IPEndPoint (IPAddress.Parse (IP), port);
		client = new UdpClient (8192);
	}

	// Set UI for sending packets
	void OnGUI ()
	{
		Rect rectObj1 = new Rect (40, 10, 200, 400);
		GUIStyle style1 = new GUIStyle ();
		style1.alignment = TextAnchor.UpperLeft;
		GUI.Box (rectObj1, "\nLast Packet: \n" + lastReceivedUDPPacket
		+ "\n\nAll Messages: \n" + allReceivedUDPPackets + "\n Receiving on port " + 8192
			, style1);

		Rect rectObj = new Rect (40, 380, 200, 400);
		GUIStyle style = new GUIStyle ();
		style.alignment = TextAnchor.UpperLeft;
		GUI.Box (rectObj, "Sending to " + IP + ":" + port
			, style);

		strMessage = GUI.TextField (new Rect (40, 420, 140, 20), strMessage);
		if (GUI.Button (new Rect (190, 420, 40, 20), "send")) {
			sendString (strMessage + "\n");
		}
	}

	// Send a string to the remote address
	private void sendString (string message)
	{
		try {
			byte[] data = Encoding.UTF8.GetBytes (message);

			client.Send (data, data.Length, remoteEndPoint);
		} catch (Exception err) {
			print (err.ToString ());
		}
	}

	// Send a continious barage of strings to the server. WARNING: Freezes application!
	private void sendEndless (string testStr)
	{
		do {
			sendString (testStr);
		} while(true);
	}

	// Starts the receiving thread for listening to packets
	private void StartReceiveThread ()
	{


		receiveThread = new Thread (
			new ThreadStart (ReceiveData));
		receiveThread.IsBackground = true;
		receiveThread.Start ();

	}

	// This is getting run over and over continiously on a different thread as to not block the rest of our application
	private  void ReceiveData ()
	{
		while (true) {
			try {
				IPEndPoint anyIP = new IPEndPoint (IPAddress.Any, 8192);

				byte[] data = client.Receive (ref anyIP);
				Debug.Log (data);
				string text = Encoding.UTF8.GetString (data);

				lastReceivedUDPPacket = text;

				allReceivedUDPPackets = allReceivedUDPPackets + text;
			} catch (Exception err) {
				print (err.ToString ());
			}
		}
	}

	// Shut down the thread and closes the client when the application quits.
	void OnApplicationQuit ()
	{
		receiveThread.Abort ();

		if (client != null) {
			client.Close ();
		}
	}

	public string getLatestUDPPacket ()
	{
		allReceivedUDPPackets = "";
		return lastReceivedUDPPacket;
	}
}
